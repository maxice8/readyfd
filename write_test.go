// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"syscall"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotifyWrite(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_WRITE")
	assert.NoError(t, err)

	// Create a pipe, we will pass the write file descriptor
	// to notify via the environment variable and use the read
	// in a goroutine
	read, write, err := os.Pipe()
	assert.NoError(t, err)

	err = os.Setenv("READY_FD_TEST_WRITE", fmt.Sprint(write.Fd()))
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_WRITE"),
		WithInput("test\n"),
	)
	assert.NoError(t, err)
	assert.Equal(t, "READY_FD_TEST_WRITE", notification.environmentVariable)

	// This goroutine will return either the input it received from
	// notify or it will fail
	resChan := make(chan string)
	errChan := make(chan error)
	go func(fd uintptr, resChan chan string, errChan chan error) {
		var str string

		file := os.NewFile(fd, "NOTIFICATION")

		reader := bufio.NewReader(file)
		str, err = reader.ReadString('\n')
		if err != nil {
			errChan <- err
			return
		}
		resChan <- str
	}(read.Fd(), resChan, errChan)

	err = notification.Notify()
	assert.NoError(t, err)

	// This will block until we receive the message
	select {
	case str := <-resChan:
		assert.Equal(t, notification.input, str) // check received is same as input
	case recvErr := <-errChan:
		assert.Error(t, recvErr)
		assert.ErrorIs(t, err, &phantomError{})
	}
}

func TestNotifyBrokenPipe(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_WRITE")
	assert.NoError(t, err)

	// Create a pipe, we will pass the write file descriptor
	// to notify via the environment variable and use the read
	// in a goroutine
	read, write, err := os.Pipe()
	assert.NoError(t, err)

	// Store it before closing
	readFD := read.Fd()
	writeFD := write.Fd()

	// Close the read so we end with a broken pipe, the write
	// will be unable to write to it and the read will get
	read.Close()

	err = os.Setenv("READY_FD_TEST_WRITE", fmt.Sprint(writeFD))
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_WRITE"),
		WithInput("test\n"),
	)
	assert.NoError(t, err)
	assert.Equal(t, "READY_FD_TEST_WRITE", notification.environmentVariable)

	// This goroutine will return either the input it received from
	// notify or it will fail
	resChan := make(chan string)
	errChan := make(chan error)
	go func(fd uintptr, resChan chan string, errChan chan error) {
		var str string

		file := os.NewFile(fd, "NOTIFICATION")

		reader := bufio.NewReader(file)
		str, err = reader.ReadString('\n')
		if err != nil {
			errChan <- err
			return
		}
		resChan <- str
	}(readFD, resChan, errChan)

	err = notification.Notify()
	assert.Error(t, err)                       // have error
	assert.NotErrorIs(t, err, &phantomError{}) // is not a phantom error

	var wErr *WriteError
	assert.ErrorIs(t, err, &WriteError{})             // is of type WriteError
	assert.ErrorAs(t, err, &wErr)                     // as WriteError
	assert.Equal(t, int(writeFD), wErr.Fd())          // WriteError contains expected value for field `fd`
	assert.Equal(t, notification.input, wErr.Input()) // WriteError contains expected value for field `input`
	assert.Equal(
		t,
		fmt.Sprintf(
			`ready: could not write to file descriptor: write %s: broken pipe`,
			fmt.Sprint(wErr.Fd()),
		),
		wErr.Error(),
	) // WriteError's message matches

	assert.ErrorIs(t, wErr.Unwrap(), syscall.EPIPE) // underlying error is of type syscall.EPIPE
	assert.Equal(
		t,
		fmt.Sprintf(`write %s: broken pipe`, fmt.Sprint(wErr.Fd())),
		wErr.Unwrap().Error(),
	) // WriteError's underlying error,

	// This will block until we receive the message
	select {
	case str := <-resChan:
		assert.Equal(t, notification.input, str) // check received is same as input
	case recvErr := <-errChan:
		assert.Error(t, recvErr)
		assert.ErrorIs(t, err, syscall.EBADF) // bad file descriptor error
	}
}

func TestNotifyWriteClose(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_WRITE")
	assert.NoError(t, err)

	// Create a pipe, we will pass the write file descriptor
	// to notify via the environment variable and use the read
	// in a goroutine
	read, write, err := os.Pipe()
	assert.NoError(t, err)

	// Store it before closing
	readFD := read.Fd()
	writeFD := write.Fd()

	write.Close()

	err = os.Setenv("READY_FD_TEST_WRITE", fmt.Sprint(writeFD))
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_WRITE"),
		WithInput("test\n"),
	)
	assert.NoError(t, err)
	assert.Equal(t, "READY_FD_TEST_WRITE", notification.environmentVariable)

	// This goroutine will return either the input it received from
	// notify or it will fail
	resChan := make(chan string)
	errChan := make(chan error)
	go func(fd uintptr, resChan chan string, errChan chan error) {
		var str string

		file := os.NewFile(fd, "NOTIFICATION")

		reader := bufio.NewReader(file)
		str, err = reader.ReadString('\n')
		if err != nil {
			errChan <- err
			return
		}
		resChan <- str
	}(readFD, resChan, errChan)

	err = notification.Notify()
	assert.Error(t, err)                       // have error
	assert.NotErrorIs(t, err, &phantomError{}) // is not a phantom error

	var wErr *WriteError
	assert.ErrorIs(t, err, &WriteError{})             // is of type WriteError
	assert.ErrorAs(t, err, &wErr)                     // as WriteError
	assert.Equal(t, int(writeFD), wErr.Fd())          // WriteError contains expected value for field `fd`
	assert.Equal(t, notification.input, wErr.Input()) // WriteError contains expected value for field `input`
	assert.Equal(
		t,
		fmt.Sprintf(
			`ready: could not write to file descriptor: write %s: bad file descriptor`,
			fmt.Sprint(wErr.Fd()),
		),
		wErr.Error(),
	) // WriteError's message matches

	assert.ErrorIs(t, wErr.Unwrap(), syscall.EBADF) // underlying error is of type syscall.EPIPE
	assert.Equal(
		t,
		fmt.Sprintf(`write %s: %v`, fmt.Sprint(wErr.Fd()), syscall.EBADF),
		wErr.Unwrap().Error(),
	) // WriteError's underlying error,

	// This will block until we receive the message
	select {
	case str := <-resChan:
		assert.Equal(t, notification.input, str) // check received is same as input
	case recvErr := <-errChan:
		assert.Error(t, recvErr)
		assert.ErrorIs(t, err, io.EOF) // EOF as we read from a file descriptor that didn't write anything
	}
}

func TestNotifyReadWriteClosed(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_WRITE")
	assert.NoError(t, err)

	// Create a pipe, we will pass the write file descriptor
	// to notify via the environment variable and use the read
	// in a goroutine
	read, write, err := os.Pipe()
	assert.NoError(t, err)

	// Store it before closing
	readFD := read.Fd()
	writeFD := write.Fd()

	// Close the read so we end with a broken pipe, the write
	// will be unable to write to it and the read will get
	write.Close()
	read.Close()

	err = os.Setenv("READY_FD_TEST_WRITE", fmt.Sprint(writeFD))
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_WRITE"),
		WithInput("test\n"),
	)
	assert.NoError(t, err)
	assert.Equal(t, "READY_FD_TEST_WRITE", notification.environmentVariable)

	// This goroutine will return either the input it received from
	// notify or it will fail
	resChan := make(chan string)
	errChan := make(chan error)
	go func(fd uintptr, resChan chan string, errChan chan error) {
		var str string

		file := os.NewFile(fd, fmt.Sprint(fd))

		reader := bufio.NewReader(file)
		str, err = reader.ReadString('\n')
		if err != nil {
			errChan <- err
			return
		}
		resChan <- str
	}(readFD, resChan, errChan)

	err = notification.Notify()
	assert.Error(t, err)                       // have error
	assert.NotErrorIs(t, err, &phantomError{}) // is not a phantom error

	var wErr *WriteError
	assert.ErrorIs(t, err, &WriteError{})             // is of type WriteError
	assert.ErrorAs(t, err, &wErr)                     // as WriteError
	assert.Equal(t, int(writeFD), wErr.Fd())          // WriteError contains expected value for field `fd`
	assert.Equal(t, notification.input, wErr.Input()) // WriteError contains expected value for field `input`
	assert.Equal(
		t,
		fmt.Sprintf(
			`ready: could not write to file descriptor: write %s: bad file descriptor`,
			fmt.Sprint(wErr.Fd()),
		),
		wErr.Error(),
	) // WriteError's message matches

	assert.ErrorIs(t, wErr.Unwrap(), syscall.EBADF) // underlying error is of type syscall.EPIPE
	assert.Equal(
		t,
		fmt.Sprintf(`write %s: %v`, fmt.Sprint(wErr.Fd()), syscall.EBADF),
		wErr.Unwrap().Error(),
	) // WriteError's underlying error,

	// This will block until we receive the message
	select {
	case str := <-resChan:
		assert.Equal(t, notification.input, str) // check received is same as input
	case recvErr := <-errChan:
		assert.Error(t, recvErr)
		assert.ErrorIs(t, err, syscall.EBADF) // EOF as we read from a file descriptor that didn't write anything
	}
}
