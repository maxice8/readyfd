// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"errors"
	"fmt"
)

var (
	ErrCreateEmptyEnvironmentVariable = errors.New("empty environment variable")
	ErrCreateEmptyInput               = errors.New("empty input")
)

// CreateError denotes the failure to create the Notification object.
type CreateError struct {
	// err denotes the error that caused |CreateError|, use `Unwrap()` to get it
	err error
}

func (e *CreateError) Error() string {
	return fmt.Sprintf("ready: %v", e.err)
}

// Unwrap returns the underlying error that triggered |CreateError|.
func (e *CreateError) Unwrap() error { return e.err }

// Is implements errors.Is for |CreateError|.
func (e *CreateError) Is(target error) bool {
	t, ok := target.(*CreateError) //nolint:errorlint // Is() methods should not unwrap errors
	if !ok {
		return false
	}
	return (t.err == e.err || t.err == nil) //nolint:goerr113,errorlint // see above
}

// NotificationOption can be used to configure
// the ready notification.
type NotificationOption func(*Notification)

// Notification is a struct with default parameters
// that must be initialized then run, it allows switching
// various parameters.
type Notification struct {
	environmentVariable string // environment variable with the file descriptor, default:  `NOTIFY_READY`.
	input               string // what to write in the file descriptor, default: '\n'.
}

// WithEnvironmentVariable sets the environment variable checked for the file descriptor
// to a custom value.
func WithEnvironmentVariable(environmentVariable string) NotificationOption {
	return func(rn *Notification) {
		rn.environmentVariable = environmentVariable
	}
}

// WithInput sets the input written to the file descriptor to a custom value.
func WithInput(delimiter string) NotificationOption {
	return func(rn *Notification) {
		rn.input = delimiter
	}
}

// NewNotification returns a notification with sane defaults.
func NewNotification(opts ...NotificationOption) (*Notification, error) {
	// Default values
	const (
		defaultEnvironmentVariable = "READY_FD"
		defaultInput               = "\n"
	)

	rd := &Notification{
		environmentVariable: defaultEnvironmentVariable,
		input:               defaultInput,
	}

	// Loop over our given custom options and set them
	for _, opt := range opts {
		opt(rd)
	}

	// Some sanity checking so the user can't do something very unwanted
	if rd.environmentVariable == "" {
		return nil, &CreateError{ErrCreateEmptyEnvironmentVariable}
	}
	if rd.input == "" {
		return nil, &CreateError{ErrCreateEmptyInput}
	}

	return rd, nil
}

// Notify tries to get the value of the environment variable defined
// withtin its' struct, then parsing it into an int and using it as
// an uintptr to open a file descriptor and finally writing into it
// and closing.
func (rd *Notification) Notify() error {
	// Lookup the environment variable and return an error
	fileDescriptorString, err := rd.GetEnvValue()
	if err != nil {
		return err
	}

	fileDescriptor, err := ParseValue(fileDescriptorString)
	if err != nil {
		return err
	}

	err = Write(fileDescriptor, rd.input)
	if err != nil {
		return err
	}

	return nil
}

// GetEnvParsed is a helper function that combines the GetEnvValue() and ParseValue()
// into a single function for purposes of ease of usage, this will return an uintptr
// or an error which can be an error from GetEnvValue() or ParseValue(), type
// assertion is required to find out which one it is.
func (rd *Notification) GetEnvParsed() (uintptr, error) {
	fdString, err := rd.GetEnvValue()
	if err != nil {
		return uintptr(0), err
	}

	fd, err := ParseValue(fdString)
	if err != nil {
		return uintptr(0), err
	}
	return fd, nil
}
