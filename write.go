// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"fmt"
	"os"
)

// Write takes an uintptr an a string, and opens a file descriptor
// based on the uintptr and writes the string to it, then closes it.
//
// In case of any failures it will return *WriteError, containing the
// underlying error, and the fd and input we tried to write.
func Write(fd uintptr, value string) error {
	file := os.NewFile(fd, fmt.Sprint(fd))
	defer file.Close()

	_, err := file.WriteString(value)
	if err != nil {
		return &WriteError{err, int(fd), value}
	}
	return nil
}

// WriteError is used when we fail to write to the file descriptor.
type WriteError struct {
	err   error  // underlying error that caused `WriteError`, get via `Unwrap()`.
	fd    int    // file descriptor we failed to write to, get via `Fd()`.
	input string // input that we tried to write as []byte, get via `Input()`.
}

func (e *WriteError) Error() string {
	return fmt.Sprintf("ready: could not write to file descriptor: %v", e.err)
}

// Unwrap returns the underlying error that triggered the WriteError.
func (e *WriteError) Unwrap() error { return e.err }

// Fd returns the file descriptor that we tried to write to.
func (e *WriteError) Fd() int { return e.fd }

// Input returns the input we tried to write to.
func (e *WriteError) Input() string { return e.input }

// Is implements errors.Is for `WriteError`.
func (e *WriteError) Is(target error) bool {
	t, ok := target.(*WriteError) //nolint:errorlint // Is() methods should not unwrap errors
	if !ok {
		return false
	}
	return (t.err == e.err || t.err == nil) && //nolint:goerr113,errorlint // see above
		(t.fd == e.fd || t.fd == 0) &&
		(t.input == e.input || t.input == "")
}
