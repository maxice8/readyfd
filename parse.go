// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"fmt"
	"strconv"
)

// ParseValue takes a string and tries to parse it into a base 10 int and then returns
// an uintptr to it, if not possible it will return a `ParseError`.
//
// In case of failure a *ParseError will be returned containing the error that caused
// it and the value we tried to parse.
func ParseValue(value string) (uintptr, error) {
	parsed, err := strconv.Atoi(value)
	if err != nil {
		return uintptr(0), &ParseError{err, value}
	}
	return uintptr(parsed), nil
}

// ParseError is used when the value given cannot be converted to an int
// and consequently to an uintptr.
type ParseError struct {
	err   error  // underlying error that caused `ParseError`, get via `Unwrap()`.
	value string // value we tried to parse into int, get via `Value()`.
}

func (e *ParseError) Error() string {
	return fmt.Sprintf(`ready: cannot parse to int "%s": %v`, e.value, e.err)
}

// Unwrap returns the underlying error that triggered the ParseError.
func (e *ParseError) Unwrap() error { return e.err }

// Value returns the value of the environment variable that was used.
func (e *ParseError) Value() string { return e.value }

// Is implements errors.Is for `ParseError`.
func (e *ParseError) Is(target error) bool {
	t, ok := target.(*ParseError) //nolint:errorlint // Is() methods should not unwrap errors
	if !ok {
		return false
	}
	return (t.err == e.err || t.err == nil) && //nolint:goerr113,errorlint // see above
		(t.value == e.value || t.value == "")
}
