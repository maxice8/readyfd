// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotifyNoEnv(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_NO_ENV")
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_NO_ENV"),
	)
	assert.NoError(t, err)

	err = notification.Notify()
	// Check that there is an error
	assert.Error(t, err)

	// Check that the error is not a phantom error
	assert.NotErrorIs(t, err, &phantomError{})

	// Check that the error is EnvNotSetError
	var eErr *EnvError
	assert.ErrorAs(t, err, &eErr)

	// Check the variable field matches what we expect
	assert.Equal(t, "READY_FD_TEST_NO_ENV", eErr.Variable())

	// Check the error message matches what we expect
	assert.Equal(t, "ready.$READY_FD_TEST_NO_ENV: environment variable not set", eErr.Error())

	// Chck that the underlying error matches what we expect
	assert.Equal(t, "environment variable not set", eErr.Unwrap().Error())
}
