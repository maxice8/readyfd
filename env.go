// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"errors"
	"fmt"
	"os"
)

var errNotSet = errors.New("environment variable not set")

// GetEnvValue gets the value of the environment variable assigned or returns nil
// an `EnvNotSetError`, it performs no validation of the value and simply returns
// the string.
//
// In case of failure a *EnvError will be returned containing the underlying error
// and the variable we tried to get the value from.
func (rd *Notification) GetEnvValue() (string, error) {
	// Lookup the environment variable and return an error
	envVar, present := os.LookupEnv(rd.environmentVariable)
	if !present {
		return "", &EnvError{rd.environmentVariable, errNotSet}
	}
	return envVar, nil
}

// EnvError is used for errors related to the environment variable.
type EnvError struct {
	// environment variable we tried to access but wasn't set, get via `Variable()`.
	variable string
	// error that caused the |EnvError|
	err error
}

func (e *EnvError) Error() string {
	return fmt.Sprintf("ready.$%s: %v", e.variable, e.err)
}

// Unwrap returns the underlying error that triggered |EnvError|.
func (e *EnvError) Unwrap() error { return e.err }

// Variable returns the variable that caused the error.
func (e *EnvError) Variable() string { return e.variable }

// Is implements errors.Is for `EnvNotSetError`.
func (e *EnvError) Is(target error) bool {
	t, ok := target.(*EnvError) //nolint:errorlint // Is() methods should not unwrap errors
	if !ok {
		return false
	}
	return (t.err == e.err || t.err == nil) && //nolint:goerr113,errorlint // see above
		(t.variable == e.variable || t.variable == "")
}
