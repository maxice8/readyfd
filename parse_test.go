// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotifyEnvEmpty(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_EMPTY_ENV")
	assert.NoError(t, err)
	err = os.Setenv("READY_FD_TEST_EMPTY_ENV", "")
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_EMPTY_ENV"),
	)
	assert.NoError(t, err)

	err = notification.Notify()
	assert.Error(t, err)                       // have error
	assert.NotErrorIs(t, err, &phantomError{}) // not phantomError

	var pErr *ParseError
	assert.ErrorIs(t, err, &ParseError{}) // is of type ParseError
	assert.ErrorAs(t, err, &pErr)         // as type ParseError
	assert.Equal(t, "", pErr.Value())     // field `value` contains ""
	assert.Equal(
		t,
		`ready: cannot parse to int "": strconv.Atoi: parsing "": invalid syntax`,
		err.Error(),
	) // ParseError's error message matches'

	assert.ErrorIs(t, pErr, strconv.ErrSyntax) // Underlying error is strconv.ErrSyntax
	assert.Equal(t,
		`strconv.Atoi: parsing "": invalid syntax`,
		pErr.Unwrap().Error(),
	) // the underlying error, strconv.ErrSyntax, matches
}

func TestNotifyEnvNotNumber(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_NOT_NUMBER_ENV")
	assert.NoError(t, err)
	err = os.Setenv("READY_FD_TEST_NOT_NUMBER_ENV", "Not A Number")
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_NOT_NUMBER_ENV"),
	)
	assert.NoError(t, err)

	err = notification.Notify()
	assert.Error(t, err)                       // have error
	assert.NotErrorIs(t, err, &phantomError{}) // not phantomError

	var pErr *ParseError
	assert.ErrorIs(t, err, &ParseError{})         // is of type ParseError
	assert.ErrorAs(t, err, &pErr)                 // as type ParseError
	assert.Equal(t, "Not A Number", pErr.Value()) // field `value` contains "Not A Number"
	assert.Equal(
		t,
		`ready: cannot parse to int "Not A Number": strconv.Atoi: parsing "Not A Number": invalid syntax`,
		err.Error(),
	) // ParseError's error message matches'

	assert.ErrorIs(t, pErr, strconv.ErrSyntax) // Underlying error is strconv.ErrSyntax
	assert.Equal(t,
		`strconv.Atoi: parsing "Not A Number": invalid syntax`,
		pErr.Unwrap().Error(),
	) // the underlying error, strconv.ErrSyntax, matches
}
