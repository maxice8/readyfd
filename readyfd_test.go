// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package readyfd

import (
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Internal testing error that should never ever appear in an errors.Is chain.
type phantomError struct{}

func (e *phantomError) Error() string { return "boo!" }

func TestDefaultValues(t *testing.T) {
	notification, err := NewNotification()

	assert.NoError(t, err)
	assert.Equal(t, "READY_FD", notification.environmentVariable)
	assert.Equal(t, "\n", notification.input)
}

func TestCustomEnvironmentVariable(t *testing.T) {
	notification, err := NewNotification(
		WithEnvironmentVariable("TEST_FD"),
	)

	assert.NoError(t, err)
	assert.Equal(t, "TEST_FD", notification.environmentVariable)
	assert.Equal(t, "\n", notification.input)
}

func TestCustomInput(t *testing.T) {
	notification, err := NewNotification(
		WithInput("test\n"),
	)

	assert.NoError(t, err)
	assert.Equal(t, "READY_FD", notification.environmentVariable)
	assert.Equal(t, "test\n", notification.input)
}

func TestCustomEnvironmentVariableAndInput(t *testing.T) {
	notification, err := NewNotification(
		WithEnvironmentVariable("TEST_FD"),
		WithInput("test\n"),
	)

	assert.NoError(t, err)
	assert.Equal(t, "TEST_FD", notification.environmentVariable)
	assert.Equal(t, "test\n", notification.input)
}

func TestCustomEnvironmentVariableEmpty(t *testing.T) {
	_, err := NewNotification(
		WithEnvironmentVariable(""),
	)
	assert.Error(t, err)
	assert.NotErrorIs(t, err, &phantomError{}) // Is not of type phantomError

	var cErr *CreateError
	assert.ErrorIs(t, err, &CreateError{}) // is of type CreateError
	assert.ErrorAs(t, err, &cErr)          // as type CreateError
	assert.Equal(t, "ready: empty environment variable", cErr.Error())
	assert.Equal(t, "empty environment variable", cErr.Unwrap().Error())
}

func TestCustomInputEmpty(t *testing.T) {
	_, err := NewNotification(
		WithInput(""),
	)
	assert.Error(t, err)
	assert.NotErrorIs(t, err, &phantomError{}) // Is not of type phantomError

	var cErr *CreateError
	assert.ErrorIs(t, err, &CreateError{}) // is of type CreateError
	assert.ErrorAs(t, err, &cErr)          // as type CreateError
	assert.Equal(t, "ready: empty input", cErr.Error())
	assert.Equal(t, "empty input", cErr.Unwrap().Error())
}

func TestGetEnvParsedNotSet(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_NO_ENV")
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_NO_ENV"),
	)
	assert.NoError(t, err)

	fd, err := notification.GetEnvParsed()
	assert.Error(t, err)                       // have error
	assert.Equal(t, uintptr(0), fd)            // due to error it returns the default value for uintptr
	assert.NotErrorIs(t, err, &phantomError{}) // Is not of type phantomError

	var eErr *EnvError
	assert.ErrorIs(t, err, &EnvError{}) // is of type EnvNotSetError
	assert.ErrorAs(t, err, &eErr)       // as type EnvNotSetError
	assert.Equal(
		t,
		"READY_FD_TEST_NO_ENV", eErr.Variable(),
	) // field `variable` contains `READY_FD_TEST_NO_ENV`
	assert.Equal(
		t,
		"ready.$READY_FD_TEST_NO_ENV: environment variable not set",
		eErr.Error(),
	) // EnvNotSetError's message matches
	assert.Equal(
		t,
		"environment variable not set",
		eErr.Unwrap().Error(),
	)
}

func TestGetEnvParsedEmpty(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_EMPTY_ENV")
	assert.NoError(t, err)
	err = os.Setenv("READY_FD_TEST_EMPTY_ENV", "")
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_EMPTY_ENV"),
	)
	assert.NoError(t, err)

	fd, err := notification.GetEnvParsed()
	assert.Error(t, err)                       // we have an error
	assert.Equal(t, uintptr(0), fd)            // due to error it returns the default value for uintptr
	assert.NotErrorIs(t, err, &phantomError{}) // Is not of type phantomError

	var pErr *ParseError
	assert.ErrorIs(t, err, &ParseError{})               // is of type ParseError
	assert.ErrorAs(t, err, &pErr)                       // as type ParseError
	assert.ErrorIs(t, pErr.Unwrap(), strconv.ErrSyntax) // Has underlying error strconv.ErrSyntax
	assert.Equal(t, "", pErr.Value())                   // field `value` contains ""
	assert.Equal(
		t,
		`ready: cannot parse to int "": strconv.Atoi: parsing "": invalid syntax`,
		err.Error(),
	) // ParseError's error message matches

	assert.Equal(t,
		`strconv.Atoi: parsing "": invalid syntax`,
		pErr.Unwrap().Error(),
	) // the underlying error, strconv.ErrSyntax, matches
}

func TestGetEnvParsedInvalidValue(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_NOT_NUMBER_ENV")
	assert.NoError(t, err)
	err = os.Setenv("READY_FD_TEST_NOT_NUMBER_ENV", "Not A Number")
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_NOT_NUMBER_ENV"),
	)
	assert.NoError(t, err)

	fd, err := notification.GetEnvParsed()
	assert.Error(t, err)                       // we have an error
	assert.Equal(t, uintptr(0), fd)            // due to error it returns the default value for uintptr
	assert.NotErrorIs(t, err, &phantomError{}) // Is not of type phantomError

	var pErr *ParseError
	assert.ErrorIs(t, err, &ParseError{})               // is of type ParseError
	assert.ErrorAs(t, err, &pErr)                       // as type ParseError
	assert.ErrorIs(t, pErr.Unwrap(), strconv.ErrSyntax) // Has underlying error strconv.ErrSyntax
	assert.Equal(t, "Not A Number", pErr.Value())       // field `value` contains "Not A Number"
	assert.Equal(
		t,
		`ready: cannot parse to int "Not A Number": strconv.Atoi: parsing "Not A Number": invalid syntax`,
		err.Error(),
	) // ParseError's error message matches

	assert.Equal(t,
		`strconv.Atoi: parsing "Not A Number": invalid syntax`,
		pErr.Unwrap().Error(),
	) // the underlying error, strconv.ErrSyntax, matches
}

func TestGetEnvParsedGood(t *testing.T) {
	err := os.Unsetenv("READY_FD_TEST_GOOD_VALUE")
	assert.NoError(t, err)
	err = os.Setenv("READY_FD_TEST_GOOD_VALUE", "5")
	assert.NoError(t, err)

	notification, err := NewNotification(
		WithEnvironmentVariable("READY_FD_TEST_GOOD_VALUE"),
	)
	assert.NoError(t, err)

	fd, err := notification.GetEnvParsed()
	assert.NoError(t, err)
	assert.Equal(t, uintptr(5), fd)
}
