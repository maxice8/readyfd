# readyfd

A library and small CLI tool for using the [READY\_FD protocol](https://gitlab.com/chinstrap/ready)

## Usage (library)

```go
import "gitlab.com/maxice8/readyfd"
```

Construct a new `Notification` with `NewNotification` and run `Notify()`.

```go
notification, err := NewNotification()
if err != nil {
    log.Fatalf("could not create notification: %v", err)
}
err = notification.Notify()
if err != nil {
    log.Fatalf("could not notify readiness: %v", err)
}
```

Custom values can be passed with an slice of `NotificationOption`.

```go
var opts []NotificationOption
// check the FOO variable instead of the default `READY_FD`
opts = append(opts, WithEnvironmentVariable("FOO"))
// Write BAR\n instead of the default `\n`
opts = append(opts, WithInput("BAR\n"))

notification, err := NewNotification(opts...)
if err != nil {
    log.Fatalf("could not create notification: %v", err)
}
```

The `Notify()` method is not the only one available, and is made by combining other methods. The following uses `GetEnvValue()` to get the value of the environment variable.

```go
envVar := "NOTIFY_SOCKET"

// Check on NOTIFY_SOCKET instead of default READY_FD
notification, err := NewNotification(WithEnvironmentVariable(envVar))
if err != nil {
    log.Fatalf("could not create notification: %v", err)
}

// Check the environment variable and return it
value, err := rd.GetEnvValue()
if err != nil {
    log.Fatalf("could not get value from the environment variable: %v", err)
}
fmt.Printf(`checked variable "%s", got "%s"`, envVar, value)
```

There is also `ParseValue()`, that tries to parse the given value into uintptr to be used as the file descriptor.

```go
value := "5"
fd, err := ParseValue(value)
if err != nil {
    log.Fatal(err)
}
fmt.Printf(`checked value "%s" got "%d"`, value, int(fd))
```

And finally `Write()`, that opens the file descriptor and writes to it.

```go
// Write foo to fd 1, which is almost always stdout.
input := "foo"
fd := uintptr(1)

err := Write(fd, input)
if err != nil {
    log.Fatal(err)
}
fmt.Printf(`wrote "%s" to fd "%d"`, input, int(fd))
```

## Usage (cli)

The `notify-ready` binary can be installed with `go install`. optionally set `GOBIN` to install to another place.

```terminal
$ go install gitlab.com/maxice8/readyfd/cmd/notify-ready
$ GOBIN=$HOME/.local/bin go install gitlab.com/maxice8/readyfd/cmd/notify-ready
```

It uses the same defaults as the library, so just running it will result in writing `\n` to the fd in `READY_FD`.

```terminal
$ notify-ready
```

Different flags can be used to change its behaviour. `-h`/`--help` will show them.

```text
Usage of /home/mx/go/bin/notify-ready:
  -env string
    	environment variable to check (default "READY_FD")
  -fd int
    	use given fd instead of checking environment variable
  -i string
    	input to write (default "\n")
```

If you want to check on a different environment variable, use `-env <STRING>`.

```terminal
$ notify-ready -env NOTIFY_SOCKET
```

If you know beforehand the fd and want to skip checking the environment variable, use `-fd <INT>`.

```terminal
$ notify-ready -fd 6
```

If you want to write something else rather than `\n`, use `-i <STRING>`.

```terminal
$ notify-ready -i FOO
```

> Flags can be combined, but using `-fd` will prevent `-env` from having any effect.
> notify-ready won't error out as to not annoy the user. 

## ToDo

- Hopefully nothing in the library side besides adding new functions as they are required
- Tests for the CLI tool
- Add example of `GetEnvParsed()` in the README

## Issues

- Report them

## Author

- Leo <thinkabit.ukim@gmail.com>

## License

This software is licensed under the Apache License, Version 2.0 (the "License"); you may not use this software except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
