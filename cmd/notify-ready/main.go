// Copyright 2021 - 2021, Leo <thinkabit.ukim@gmail.com> and the readyfd contributors
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"errors"
	"flag"
	"log"

	"gitlab.com/maxice8/readyfd"
)

func main() {
	// All our flags.
	var (
		iFlag   = flag.String("i", "\n", "input to write")
		envFlag = flag.String("env", "READY_FD", "environment variable to check")
		fdFlag  = flag.Int("fd", 0, "use given fd instead of checking environment variable")
	)

	input := iFlag
	envvar := envFlag
	fd := fdFlag

	flag.Parse()

	// Only print prefix
	log.SetFlags(log.Lmsgprefix)
	log.SetPrefix("notify-ready: ")

	// Check if were not given a custom fd and get it from the environment
	if *fd != 0 {
		err := readyfd.Write(uintptr(*fd), *input)
		var wErr *readyfd.WriteError
		errors.As(err, &wErr)
		log.Fatal(wErr.Unwrap())
	}

	var opts []readyfd.NotificationOption

	opts = append(opts, readyfd.WithEnvironmentVariable(*envvar), readyfd.WithInput(*input))

	notification, err := readyfd.NewNotification(opts...)
	if err != nil {
		var cErr *readyfd.CreateError
		errors.As(err, &cErr)
		log.Fatal(cErr.Unwrap())
	}

	err = notification.Notify()
	if err != nil {
		var eErr *readyfd.EnvError
		if errors.As(err, &eErr) {
			log.Fatal(eErr.Unwrap())
		}
		var wErr *readyfd.WriteError
		if errors.As(err, &wErr) {
			log.Fatal(wErr.Unwrap())
		}
	}
}
